/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
 * Author Ryan Guarascia
 * Class COMP-10062-03-W16-NC
 * Date: 
 * Description: 
 * 
 * 
 */
package lab5;

import com.sun.xml.internal.ws.util.StringUtils;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rguarascia
 */
public class Cruncher {

    private final String inputPath;
    private final String outputPath;
    private final ArrayList<String> lines = new ArrayList<>();
    private double Lat1;
    private double Lat2;
    private double Long2;
    private double Long1;
    FileInputStream fsi = null;
    FileOutputStream fso = null;
    File outputFile = null;
    BufferedReader buffRead = null;
    Writer writer = null;
    Scanner input = new Scanner(System.in);
    String[] inspectMe = new String[4];
    int[] city1 = new int[2];
    boolean flipflop = true;

    public Cruncher(String in, String out) {
        this.inputPath = in;
        this.outputPath = out;
    }

    public void openFiles() {
        try {
            fsi = new FileInputStream(inputPath);
            buffRead = new BufferedReader(new InputStreamReader(fsi));
            String temp;
            while ((temp = buffRead.readLine()) != null) {
                lines.add(temp);
            }
            Collections.sort(lines, String.CASE_INSENSITIVE_ORDER);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cruncher.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("File not found.");
        } catch (SecurityException ex) {
            Logger.getLogger(Cruncher.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("File not accessible. Denied premissions");
        } catch (IOException ex) {
            Logger.getLogger(Cruncher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Finds the line where the city and province are
    public String getLineHelper(ArrayList<String> list, String city, String province) {
        String correctLine = "";
        for (String x : list) {
            inspectMe = x.split(":");
            if (inspectMe[0].equalsIgnoreCase(province) && inspectMe[1].equalsIgnoreCase(city)) {
                correctLine = "";
                correctLine = x;
                break;
            }
        }
        return correctLine;
    }

    public void findCityHelper() {
        //Gets first city
        System.out.println("Enter a city: ");
        String city = input.nextLine();
        System.out.println("Enter province: ");
        String province = input.nextLine();
        String correctLine1 = getLineHelper(lines, city, province);
        inspector(correctLine1);

        //Gets second city
        System.out.println("Enter another city: ");
        city = input.nextLine();
        System.out.println("Enter another province: ");
        province = input.nextLine();
        String correctLine2 = getLineHelper(lines, city, province);
        inspector(correctLine2);

        System.out.println(correctLine1);
        System.out.println(correctLine2);
        System.out.println("LONG1:" + Long1);
        System.out.println("LAT1:" + Lat1);
        System.out.println("LONG2:" + Long2);
        System.out.println("LAT2:" + Lat2);
    }

    public void inspector(String line) {
        String[] strCut = line.split(":");
        try {
            strCut[2] = strCut[2].replaceAll("\\u00b0", "").replaceAll("'", "").replaceAll("N", "").replaceFirst("W", "");
            strCut[3] = strCut[3].replaceAll("\\u00b0", "").replaceAll("'", "").replaceAll("N", "").replaceFirst("W", "");
        } catch (Exception e) {
            System.out.println("The City was not found.");
            System.exit(0);
        }
        String[] temp = strCut[2].split(" ");
        if (flipflop) {
            Long1 = Math.toRadians(Integer.parseInt(temp[0]) + ((double) Integer.parseInt(temp[1]) / 60));
            temp = strCut[3].split(" ");
            Lat1 = Math.toRadians(Integer.parseInt(temp[0]) + ((double) Integer.parseInt(temp[1]) / 60));
            flipflop = !flipflop;
        } else {
            Long2 = Math.toRadians(Integer.parseInt(temp[0]) + ((double) Integer.parseInt(temp[1]) / 60));
            temp = strCut[3].split(" ");
            Lat2 = Math.toRadians(Integer.parseInt(temp[0]) + ((double) Integer.parseInt(temp[1]) / 60));
        }
    }

    public void findDistance() {
        findCityHelper();
        double earthRadius = 6371;   // in km
        double distance = Math.acos(Math.sin(Lat1) * Math.sin(Lat2)
                + Math.cos(Lat1) * Math.cos(Lat2)
                * Math.cos(Long2 - Long1)) * earthRadius;
        System.out.println("Total Distance from the cities is: " + distance);
    }

    public void writeRecords() {
        try {
            fso = new FileOutputStream(outputPath);
            try {
                writer = new BufferedWriter(new OutputStreamWriter(fso, StandardCharsets.UTF_8));
                for (String x : lines) {
                    writer.write(x + System.getProperty("line.separator"));
                }
            } catch (IOException ex) {
                Logger.getLogger(Cruncher.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cruncher.class.getName()).log(Level.SEVERE, null, ex);
            // you cant handle me
        }
    }

    public void closeFiles() {
        try {
            fsi.close();
            buffRead.close();
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(Cruncher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
